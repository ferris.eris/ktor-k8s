package com.freelance.samples.docker.main

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.html.*
import io.ktor.routing.*
import kotlinx.html.*

import io.ktor.client.HttpClient
import io.ktor.client.engine.apache.Apache
//import io.ktor.client.cio.CIO
import io.ktor.client.request.*
import kotlinx.coroutines.experimental.*

fun Application.main() {
    install(DefaultHeaders)
    install(CallLogging)
    routing {
        get("/") {
            call.respondHtml {
                head {
                    title { +"Ktor: Docker" }
                }
                body {
                    val client = HttpClient( Apache )

                    val response1 = runBlocking {
                        client.get<String>(scheme = "http", host = "localhost", port = 8081)
                    }
                    p {+"${response1}"}

                    val response2 = runBlocking {
                        client.get<String>(scheme = "http", host = "localhost", port = 8082)
                    }
                    p {+"${response2}"}

                    val response3 = runBlocking {
                        client.get<String>(scheme = "http", host = "localhost", port = 8083)
                    }
                    p {+"${response3}"}
                }
            }
        }
    }
}
