# A demo of my DevOps to GCP
- 4 containers in a pod
- programs are written in Kotlin w/ Ktor web framework

## Preparation
- create a Kubernetes cluster with GitLab (Operations -> Kubernetes)
    - n1-standard1 may be needed
    - n1-standard2 may be needed if you install Helm & Prometheus (Ingress cannot be installed)
