package com.freelance.samples.docker.p001

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.html.*
import io.ktor.routing.*
import io.ktor.response.*
import kotlinx.html.*

fun Application.main() {
    install(CallLogging)
    routing {
        get("/") {
            call.respondText(p001_2(1, 1000).toString())
        }
    }
}
